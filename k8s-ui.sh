echo "Docker Desktop must be running..."
echo "Access k8s UI at localhost:8441"

kubectl -n kubernetes-dashboard port-forward svc/kubernetes-dashboard-kong-proxy 8441:443

./compose-stop.sh
# Refresh defined file content
mkdir -p nodelab/db
# Setup database
docker compose -f docker-compose-db.yml pull
docker compose -f docker-compose-db.yml up -d
# Sleep for a few seconds to allow database to initialize
sleep 10
./compose-stop.sh

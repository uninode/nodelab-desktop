echo
echo Shutting down current server
./compose-stop.sh
echo
echo Cloning new deployment
rm -rf nodelab-deploy-demo
git clone https://gitlab.com/uninode/nodelab-deploy-demo.git
echo
echo Removing old deployment
rm -rf nodelab
mv ./nodelab-deploy-demo ./nodelab
cd nodelab
echo
echo Cloning configuration
git clone https://gitlab.com/uninode/nodelab-config-demo.git
mv nodelab-config-demo config
mkdir -p node/com.nodelab.deploy.Site
cd node/com.nodelab.deploy.Site
echo
echo Cloning demo site
git clone https://gitlab.com/uninode/nodelab-site-demo.git
mv Nodelab-site-demo demo
cd ../../..
echo
echo Initializing database, Docker Desktop must be running...
./compose-install.sh
echo
echo Starting server
./compose-restart.sh
